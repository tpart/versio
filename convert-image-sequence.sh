#!/bin/bash
ffmpeg -framerate 24 -pattern_type glob -i '*.png' \
  -c:v libvpx -pix_fmt yuv420p output-noaudio.mkv
ffmpeg -i output-noaudio.mkv -i audio.ogg -c copy -map 0:v:0 -map 1:a:0 output.mkv