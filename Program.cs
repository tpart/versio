﻿using Ookii.CommandLine;

namespace Versio
{
    [ParseOptions(Mode = ParsingMode.LongShort, ArgumentNameTransform = NameTransform.DashCase)]
    class Arguments
    {
        [CommandLineArgument(Position = 0, ShortName = 'e', ValueDescription = "The directory of the input image sequence")]
        public DirectoryInfo? Encode { get; set; }

        [CommandLineArgument(Position = 1, ShortName = 'o', ValueDescription = "The output file / directory for encoding / decoding")]
        public string? Output { get; set; }

        [CommandLineArgument(Position = 2, ShortName = 'a', ValueDescription = "The audio file that will be packed along with the video")]
        public FileInfo? Audio { get; set; }

        [CommandLineArgument(Position = 0)]
        public FileInfo? Decode { get; set; }

        [CommandLineArgument(ShortName = 'p', ValueDescription = "If true, generates a preview of the decoded image sequence while encoding the file. This will have a minimal performance impact.")]
        public bool GeneratePreview { get; set; }

        [CommandLineArgument(ShortName = 'd', ValueDescription = "If true, adds debug information to the preview images")]
        public bool DebugPreview { get; set; }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Arguments? arguments = CommandLineParser.Parse<Arguments>();

            if (arguments == null)
            {
                return;
            }

            if (arguments.Encode == null && arguments.Decode == null)
            {
                Console.WriteLine("Please specify if you want to encode a video (using --encode) or decode a vio file (using --decode). Use the --help argument for more information.");
            }

            if (arguments.Encode != null)
            {
                if (arguments.Encode.Exists)
                {
                    FileInfo[] dirFiles = arguments.Encode.GetFiles();
                    Array.Sort(dirFiles, (f1, f2) => f1.Name.CompareTo(f2.Name));

                    FileInfo outputFile = new FileInfo(@"output.vio");

                    if (arguments.Output != null)
                    {
                        outputFile = new FileInfo(arguments.Output);
                    }

                    if (outputFile.Exists)
                    {
                        outputFile.Delete();
                    }

                    Encoder.Encode(dirFiles, outputFile, arguments.Audio, arguments.GeneratePreview, arguments.DebugPreview);
                }
                else
                {
                    throw new DirectoryNotFoundException();
                }
            }

            if (arguments.Decode != null)
            {
                if (arguments.Decode.Exists)
                {
                    DirectoryInfo outputDir = new DirectoryInfo(@"output");
                    if (arguments.Output != null)
                    {
                        outputDir = new DirectoryInfo(arguments.Output);
                    }
                    if (!outputDir.Exists)
                    {
                        outputDir.Create();
                    }
                    Decoder.Decode(arguments.Decode, outputDir);
                }
                else
                {
                    throw new FileNotFoundException($"Could not find file: {arguments.Decode}");
                }
            }
        }
    }
}