# Versio file specification

1. Magic bytes:

179; 116; 142; 24

2. Start GZIP

- i32 Image width
- i32 Image height
- i32 Macroblock width / height (always square shaped)
- i32 Frame count

3. Begin main file

This part of the file has to be looped until the file ends.

- byte type (Describes what kind of data follows)

## type 0:

I-Frame (jpeg image of full frame)

- i32 Data length
- byte[] Data

## type 1 or 2:

Start reading motion / pixel areas.
This part of the file contains updates for (Image width / Macroblock width) * (Image height / Macroblock height) areas.
They are either pixel areas (type 1) which update all pixels in the current area or motion areas (type 2) which only describes movement of the pixels in the current area. The following pseudo-code can be used to calculate the area positions (position of the top left corner of the area) in the image.

```c#
// First of all, calculate area offsets, which will be used later
Vector2[] areaOffsets = new Vector2[(height / macroblockSize) * (width / macroblockSize)];

int areaIdx = 0;
for (int y = 0; y < height; y += macroblockSize)
{
	for (int x = 0; x < width; x += macroblockSize)
	{
		areaOffsets[areaIdx] = new Vector2(x, y);
		areaIdx++;
	}
}
```

Finally, the following pseudo-code can be used during the main file part (type 1 or 2) to update the current image:

```c#
// Read motion / pixel updates
byte currentByte = input.ReadByte();
int imageAreas = (width / motionRectSize) * (height / motionRectSize);
byte[] areaTransforms = new byte[imageAreas];

for (int i = 0; i < imageAreas; i++)
{
	if (currentByte == 1)
	{
		// Read pixel update
		int length = ToInt32(input.ReadBytes(4));
		byte[] image = input.ReadBytes(length);
		Vector2 areaOffset = areaOffsets[i];
		Image<Rgb24> area = (Image<Rgb24>)JpegDec.Decode(image);

		for (int y = 0; y < area.Width; y++)
		{
			for (int x = 0; x < area.Height; x++)
			{
				currentImage[x + areaOffset.X, y + areaOffset.Y] = area[x, y];
			}
		}

		areaTransforms[i] = 136; // 136 means no movement (Vector2(0, 0))
	}
	else if (next == 2)
	{
		// Read motion update
		int direction = input.ReadByte();
		areaTransforms[i] = (byte)direction;
	}
	else
	{
		throwError("Invalid vio file!");
	}

	currentByte = input.ReadByte();
}
```

Next, the area transforms in the areaTransforms array have to be applied to the image. Each byte corresponds to a motion vector, as described in more detail below in type 2. For more information, take a look at the ApplyAreaTransforms function in the reference implementation.

### type 1:

Pixel area (jpeg image of current area)

- i32 Data length
- byte[] Data

### type 2:

Motion area

- byte Motion direction

Motion direction vectors can be generated using the following pseudo-code:

```c#
const int maxMovementPixels = 8; // maxMovementPixels is always 8
Vector2[] directions = new Vector2[maxMovementPixels * 4 * maxMovementPixels];

int directionIdx = 0;
for (int y = -maxMovementPixels; y < maxMovementPixels; y++)
{
	for (int x = -maxMovementPixels; x < maxMovementPixels; x++)
	{
		directions[directionIdx] = new Vector2(x, y);
		directionIdx++;
	}
}
```


## type 3:

Audio data

- i32 Data length
- byte[] Data