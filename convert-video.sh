#!/bin/bash

# This is a bash script that can convert a video to an image sequence (which can be encoded by versio)
ffmpeg -i video.mp4 -f image2 video/image-%07d.png
ffmpeg -i video.mp4 -vn audio.ogg