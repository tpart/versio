using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Formats.Jpeg;
using System.IO.Compression;
using ShellProgressBar;

namespace Versio
{
    public class Decoder
    {
        public static int MaxMovementPixels = 8;
        public static JpegDecoder JpegDec = new JpegDecoder();
        public static Vector2[] Directions = new Vector2[MaxMovementPixels * 4 * MaxMovementPixels];
        public static Vector2[] NeighbourPixel = new Vector2[] { new Vector2(-1, 0), new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, -1) };
        private static ProgressBarOptions _mainProgressOptions = new ProgressBarOptions { ForegroundColor = ConsoleColor.White, ForegroundColorDone = ConsoleColor.Green, BackgroundColor = ConsoleColor.DarkGray, ProgressBarOnBottom = false };

        public static void Decode(FileInfo inputFiles, DirectoryInfo outputDirectory)
        {
            int directionIdx = 0;
            for (int y = -MaxMovementPixels; y < MaxMovementPixels; y++)
            {
                for (int x = -MaxMovementPixels; x < MaxMovementPixels; x++)
                {
                    Directions[directionIdx] = new Vector2(x, y);
                    directionIdx++;
                }
            }

            FileStream fileStream = inputFiles.OpenRead();

            // Read magic bytes

            byte[] magicBytes = new byte[4];
            fileStream.ReadExactly(magicBytes, 0, 4);
            if (magicBytes[0] != 179 || magicBytes[1] != 116 || magicBytes[2] != 142 || magicBytes[3] != 24)
            {
                throw new Exception($"Could not open file: Not a valid vio file (Magic bytes check failed: {magicBytes[0]}, {magicBytes[1]}, {magicBytes[2]}, {magicBytes[3]})");
            }

            GZipStream input = new GZipStream(fileStream, CompressionMode.Decompress, false);

            byte[] widthBytes = new byte[4];
            byte[] heightBytes = new byte[4];
            byte[] motionRectSizeBytes = new byte[4];
            byte[] frameCountBytes = new byte[4];
            input.ReadExactly(widthBytes, 0, 4);
            input.ReadExactly(heightBytes, 0, 4);
            input.ReadExactly(motionRectSizeBytes, 0, 4);
            input.ReadExactly(frameCountBytes, 0, 4);
            int width = BitConverter.ToInt32(widthBytes);
            int height = BitConverter.ToInt32(heightBytes);
            int motionRectSize = BitConverter.ToInt32(motionRectSizeBytes);
            int frameCount = BitConverter.ToInt32(frameCountBytes);
            int imageAreas = (width / motionRectSize) * (height / motionRectSize);

            ProgressBar progress = new ProgressBar(frameCount, "Encoding video", _mainProgressOptions);

            // Calculate area offsets
            Vector2[] areaOffsets = new Vector2[(height / motionRectSize) * (width / motionRectSize)];

            {
                int areaIdx = 0;
                for (int y = 0; y < height; y += motionRectSize)
                {
                    for (int x = 0; x < width; x += motionRectSize)
                    {
                        areaOffsets[areaIdx] = new Vector2(x, y);
                        areaIdx++;
                    }
                }
            }

            int currentByte = input.ReadByte();

            Image<Rgb24> currentImage = new Image<Rgb24>(Configuration.Default, width, height);

            int currentImageIdx = 0;
            while (currentByte != -1)
            {
                if (currentByte == 0)
                {
                    progress.Message = $"Frame {progress.CurrentTick}; Reading I-Frame...";
                    Console.WriteLine("Reading I-Frame");
                    // Read I-Frame
                    byte[] lengthBytes = new byte[4];
                    input.ReadExactly(lengthBytes, 0, 4);
                    int length = BitConverter.ToInt32(lengthBytes);
                    byte[] image = new byte[length];
                    input.ReadExactly(image, 0, length);
                    MemoryStream imageStream = new MemoryStream(image);

                    currentImage = (Image<Rgb24>)JpegDec.Decode(Configuration.Default, imageStream, new CancellationToken());

                    currentImage.SaveAsPng(Path.Combine(outputDirectory.FullName, $"{currentImageIdx.ToString("D4")}.png"));

                    currentByte = input.ReadByte();
                }
                else if (currentByte == 1 || currentByte == 2)
                {
                    progress.Message = $"Frame {progress.CurrentTick}; Reading motion / pixel update...";
                    // Read motion update / pixel update
                    byte[] areaTransforms = new byte[imageAreas];

                    int next = currentByte;

                    for (int i = 0; i < imageAreas; i++)
                    {
                        if (next == 1)
                        {
                            // Read pixel update
                            byte[] lengthBytes = new byte[4];
                            input.ReadExactly(lengthBytes, 0, 4);
                            int length = BitConverter.ToInt32(lengthBytes);
                            byte[] image = new byte[length];
                            input.ReadExactly(image, 0, length);
                            MemoryStream imageStream = new MemoryStream(image);
                            Image<Rgb24> area = (Image<Rgb24>)JpegDec.Decode(Configuration.Default, imageStream, new CancellationToken());
                            Vector2 areaOffset = areaOffsets[i];

                            for (int y = 0; y < area.Width; y++)
                            {
                                for (int x = 0; x < area.Height; x++)
                                {
                                    currentImage[x + areaOffset.X, y + areaOffset.Y] = area[x, y];
                                }
                            }

                            areaTransforms[i] = 136;
                        }
                        else if (next == 2)
                        {
                            // Read motion update
                            int direction = input.ReadByte();
                            if (direction == -1)
                            {
                                throw new Exception("Unexpected end of file");
                            }
                            areaTransforms[i] = (byte)direction;
                        }
                        else
                        {
                            throw new NotSupportedException($"The vio file contains invalid data types. The file may be broken or may have been encoded using a newer version of versio. Byte: {currentByte}");
                        }
                        next = input.ReadByte();
                    }
                    
                    currentImage = ApplyAreaTransforms(currentImage, areaTransforms, motionRectSize);

                    currentImage.SaveAsPng(Path.Combine(outputDirectory.FullName, $"{currentImageIdx.ToString("D4")}.png"));

                    currentByte = next;
                }
                else if (currentByte == 3)
                {
                    progress.Message = $"Frame {progress.CurrentTick}; Reading audio...";
                    byte[] audioLengthBytes = new byte[4];
                    input.ReadExactly(audioLengthBytes, 0, 4);
                    int audioLength = BitConverter.ToInt32(widthBytes);
                    byte[] audioBytes = new byte[audioLength];
                    input.ReadExactly(audioBytes, 0, audioLength);
                    File.WriteAllBytes("audio.ogg", audioBytes);
                }
                else
                {
                    throw new NotSupportedException($"The vio file contains invalid data types. The file may be broken or may have been encoded using a newer version of versio. Byte: {currentByte}");
                }

                currentImageIdx++;

                progress.Tick();
            }

            input.Close();

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Done.");
        }

        public static Image<Rgb24> ApplyAreaTransforms(Image<Rgb24> inputImage, byte[] areaDirections, int motionRectSize)
        {
            Image<Rgb24> outputImage = new Image<Rgb24>(Configuration.Default, inputImage.Width, inputImage.Height);
            int areaIdx = 0;
            int imageHeight = inputImage.Height;
            int imageWidth = inputImage.Width;

            bool[] definedPixels = new bool[imageHeight * imageWidth];

            for (int i = 0; i < definedPixels.Length; i++)
            {
                definedPixels[i] = false;
            }

            for (int y = 0; y < imageHeight; y += motionRectSize)
            {
                for (int x = 0; x < imageWidth; x += motionRectSize)
                {
                    byte areaDirection = areaDirections[areaIdx];
                    Vector2 movementVector = Directions[areaDirection];
                    for (int ym = y; ym < y + motionRectSize; ym++)
                    {
                        for (int xm = x; xm < x + motionRectSize; xm++)
                        {
                            int movedXm = xm + movementVector.X;
                            int movedYm = ym + movementVector.Y;

                            if (movedXm < 0 || movedYm < 0 || movedXm >= imageWidth - 1 || movedYm >= imageHeight - 1)
                            {
                                continue;
                            }
                            else
                            {
                                Vector2 position = new Vector2(movedXm, movedYm);
                                int pixelIdx = movedXm + imageWidth * movedYm;
                                if (definedPixels[pixelIdx] == false)
                                {
                                    outputImage[movedXm, movedYm] = inputImage[xm, ym];
                                    definedPixels[pixelIdx] = true;
                                }
                                else
                                {
                                    outputImage[movedXm, movedYm] = Encoder.MixPixels(inputImage[xm, ym], outputImage[movedXm, movedYm]);
                                }
                            }
                        }
                    }
                    areaIdx++;
                }
            }

            // Fill in empty pixels
            int idx = 0;

            for (int y = 0; y < imageHeight; y++)
            {
                for (int x = 0; x < imageWidth; x++)
                {
                    if (definedPixels[idx] == false)
                    {
                        if (x > 1 && x < imageWidth - 1 && y > 1 && y < imageHeight - 1)
                        {
                            // Check avg value of neighbour pixels
                            List<Rgb24> neighbourPixelColors = new List<Rgb24>();
                            for (int n = 0; n < NeighbourPixel.Length; n++)
                            {
                                Vector2 vector2 = NeighbourPixel[n];
                                if (definedPixels[(x + vector2.X) + (y + vector2.Y) * imageWidth] == true)
                                {
                                    neighbourPixelColors.Add(outputImage[x + vector2.X, y + vector2.Y]);
                                }
                            }
                            if (neighbourPixelColors.Count > 0)
                            {
                                outputImage[x, y] = Encoder.MixPixels(neighbourPixelColors);
                            }
                            else
                            {
                                outputImage[x, y] = inputImage[x, y];
                            }
                        }
                        else
                        {
                            outputImage[x, y] = inputImage[x, y];
                        }
                    }
                    idx++;
                }
            }
            
            return outputImage;
        }
    }
}