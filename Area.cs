using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;


namespace Versio
{
    public class Area
    {
        public Rgb24[] Data;
        public int OffsetX;
        public int OffsetY;

        public Area(Rgb24[] Data)
        {
            this.Data = Data;
        }

        public Area(Rgb24[] Data, int OffsetX, int OffsetY)
        {
            this.Data = Data;
            this.OffsetX = OffsetX;
            this.OffsetY = OffsetY;
        }

        public Rgb24? GetPixel(int x, int y, int rectSize)
        {
            if (x < 0 || y < 0 || x > rectSize - 1 || y > rectSize - 1)
            {
                return null;
            }
            return Data[y * rectSize + x];
        }
    }
}