using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Formats.Jpeg;
using System.IO.Compression;
using System.Linq;
using ShellProgressBar;

namespace Versio
{
    public class Encoder
    {
        public static int MotionRectSize = 40;
        public static int MaxMovementPixels = 8;
        public static int MaxAreaDifference = 14500;
        public static JpegEncoder JpegEnc = new JpegEncoder();
        public static JpegDecoder JpegDec = new JpegDecoder();
        public static Vector2[] Directions = new Vector2[MaxMovementPixels * 4 * MaxMovementPixels];
        public static Vector2[] NeighbourPixel = new Vector2[] { new Vector2(-1, 0), new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, -1) };
        private static ProgressBarOptions _mainProgressOptions = new ProgressBarOptions { ForegroundColor = ConsoleColor.White, ForegroundColorDone = ConsoleColor.Green, BackgroundColor = ConsoleColor.DarkGray, ProgressBarOnBottom = false };
        private static ProgressBarOptions _childProgressOptions = new ProgressBarOptions { ProgressCharacter = '─', ForegroundColor = ConsoleColor.White, ForegroundColorDone = ConsoleColor.Green, BackgroundColor = ConsoleColor.DarkGray, ProgressBarOnBottom = false, CollapseWhenFinished = true };

        public static void Encode(FileInfo[] inputFiles, FileInfo outputFile, FileInfo? audioFile = null, bool generatePreview = false, bool debugPreview = true)
        {
            FileStream fileStream = outputFile.OpenWrite();

            // Write magic bytes
            fileStream.WriteByte(179);
            fileStream.WriteByte(116);
            fileStream.WriteByte(142);
            fileStream.WriteByte(24);

            GZipStream output = new GZipStream(fileStream, CompressionMode.Compress, false);

            Image firstImage = GetImage(inputFiles[0]);
            int imageWidth = firstImage.Width;
            int imageHeight = firstImage.Height;

            if (imageHeight % MotionRectSize != 0 || imageWidth % MotionRectSize != 0)
            {
                Console.WriteLine($"Error: Motion rect size of {MotionRectSize} is not suitable for video of resolution {imageHeight} * {imageWidth}");
            }

            ProgressBar progress = new ProgressBar(inputFiles.Length, "Encoding video", _mainProgressOptions);
            JpegEnc.Quality = 25;

            byte[] width = BitConverter.GetBytes(firstImage.Width).ToArray();
            byte[] height = BitConverter.GetBytes(firstImage.Height).ToArray();
            byte[] motionRectSizeBytes = BitConverter.GetBytes(MotionRectSize).ToArray();
            byte[] frameCountBytes = BitConverter.GetBytes(inputFiles.Length).ToArray();
            output.Write(width);
            output.Write(height);
            output.Write(motionRectSizeBytes);
            output.Write(frameCountBytes);

            // Calculate area directions
            int directionIdx = 0;
            for (int y = -MaxMovementPixels; y < MaxMovementPixels; y++)
            {
                for (int x = -MaxMovementPixels; x < MaxMovementPixels; x++)
                {
                    Directions[directionIdx] = new Vector2(x, y);
                    directionIdx++;
                }
            }

            Image<Rgb24> currentImage = GetImage(inputFiles[0]);

            WriteIFrame(output, currentImage);
            progress.Tick();

            // Calculate area offsets
            Vector2[] areaOffsets = new Vector2[(imageHeight / MotionRectSize) * (imageWidth / MotionRectSize)];

            {
                int areaIdx = 0;
                for (int y = 0; y < imageHeight; y += MotionRectSize)
                {
                    for (int x = 0; x < imageWidth; x += MotionRectSize)
                    {
                        areaOffsets[areaIdx] = new Vector2(x, y);
                        areaIdx++;
                    }
                }
            }


            for (int i = 1; i < inputFiles.Length; i++)
            {
                Image<Rgb24> img = GetImage(inputFiles[i]);

                // Calculate areas for old and new image
                int imgAreaSize = (imageHeight / MotionRectSize) * (imageWidth / MotionRectSize);
                Area[] oldImgAreas = new Area[imgAreaSize];
                Area[] newImgAreas = new Area[imgAreaSize];

                {
                    int areaIdx = 0;
                    for (int y = 0; y < imageHeight; y += MotionRectSize)
                    {
                        for (int x = 0; x < imageWidth; x += MotionRectSize)
                        {
                            Rgb24[] areaData = new Rgb24[MotionRectSize * MotionRectSize];
                            int j = 0;
                            for (int areaY = y; areaY < y + MotionRectSize; areaY++)
                            {
                                for (int areaX = x; areaX < x + MotionRectSize; areaX++)
                                {
                                    Rgb24 color = currentImage[areaX, areaY];
                                    areaData[j] = color;
                                    j++;
                                }
                            }
                            oldImgAreas[areaIdx] = new Area(areaData, x, y);
                            areaIdx++;
                        }
                    }
                }

                {
                    int areaIdx = 0;
                    for (int y = 0; y < imageHeight; y += MotionRectSize)
                    {
                        for (int x = 0; x < imageWidth; x += MotionRectSize)
                        {
                            Rgb24[] areaData = new Rgb24[MotionRectSize * MotionRectSize];
                            int j = 0;
                            for (int areaY = y; areaY < y + MotionRectSize; areaY++)
                            {
                                for (int areaX = x; areaX < x + MotionRectSize; areaX++)
                                {
                                    Rgb24 color = img[areaX, areaY];
                                    areaData[j] = color;
                                    j++;
                                }
                            }
                            newImgAreas[areaIdx] = new Area(areaData, x, y);
                            areaIdx++;
                        }
                    }
                }

                // Compare area movement
                byte[] areaDirections = new byte[newImgAreas.Length];
                Task<(byte, Vector2)>[] tasks = new Task<(byte, Vector2)>[newImgAreas.Length];
                List<Vector2> vectors = new List<Vector2>();

                for (int t = 0; t < newImgAreas.Length; t++)
                {
                    Area oldArea = oldImgAreas[t];
                    Area newArea = newImgAreas[t];
                    Task<(byte, Vector2)> task = Task.Run(() => CalculateBestAreaDirection(oldArea, newArea, currentImage, img));

                    tasks[t] = task;
                }

                ChildProgressBar child = progress.Spawn(tasks.Length, "Calculating P-Frame", _childProgressOptions);

                for (int t = 0; t < tasks.Length; t++)
                {
                    tasks[t].Wait();
                    (byte, Vector2) bestDirection = tasks[t].Result;
                    areaDirections[t] = bestDirection.Item1;
                    vectors.Add(bestDirection.Item2);
                    child.Tick();
                }

                currentImage = ApplyAreaTransforms(currentImage, areaDirections);

                // Calculate areas for new transformed image

                Area[] transformedImgAreas = new Area[imgAreaSize];

                {
                    int areaIdx = 0;
                    for (int y = 0; y < imageHeight; y += MotionRectSize)
                    {
                        for (int x = 0; x < imageWidth; x += MotionRectSize)
                        {
                            Rgb24[] areaData = new Rgb24[MotionRectSize * MotionRectSize];
                            int j = 0;
                            for (int areaY = y; areaY < y + MotionRectSize; areaY++)
                            {
                                for (int areaX = x; areaX < x + MotionRectSize; areaX++)
                                {
                                    Rgb24 color = currentImage[areaX, areaY];
                                    areaData[j] = color;
                                    j++;
                                }
                            }
                            transformedImgAreas[areaIdx] = new Area(areaData);
                            areaIdx++;
                        }
                    }
                }

                // Check which areas should be updated
                List<int> updateAreas = new List<int>();

                for (int a = 0; a < transformedImgAreas.Length; a++)
                {
                    int diff = AreaDiff(newImgAreas[a], transformedImgAreas[a]);
                    float weight = CalculateWeight(transformedImgAreas[a]);
                    int weightedDiff = (int)((float)diff / weight);
                    if (weightedDiff > MaxAreaDifference)
                    {
                        updateAreas.Add(a);
                    }
                }

                // Write movement and updates to output
                int motionUpdates = 0;
                int pixelUpdates = 0;
                for (int a = 0; a < transformedImgAreas.Length; a++)
                {
                    if (updateAreas.Contains(a))
                    {
                        // Write pixel update
                        Area originalArea = newImgAreas[a];
                        MemoryStream updateStream = new MemoryStream();

                        Image<Rgb24> areaImage = new Image<Rgb24>(Configuration.Default, MotionRectSize, MotionRectSize);

                        int idx = 0;

                        for (int y = 0; y < MotionRectSize; y++)
                        {
                            for (int x = 0; x < MotionRectSize; x++)
                            {
                                Rgb24 pixel = originalArea.Data[idx];
                                Vector2 areaOffset = areaOffsets[a];
                                currentImage[x + areaOffset.X, y + areaOffset.Y] = pixel;
                                areaImage[x, y] = pixel;
                                idx++;
                            }
                        }

                        JpegEnc.Encode(areaImage, updateStream);

                        // Update simulation
                        /* Note: This WILL increase file size because compressed frames are compared to uncompressed frames. This only exists for debugging and testing purposses.
                        updateStream.Position = 0;

                        Image<Rgb24> simulation = (Image<Rgb24>)JpegDec.Decode(Configuration.Default, updateStream, new CancellationToken());

                        for (int y = 0; y < MotionRectSize; y++)
                        {
                            for (int x = 0; x < MotionRectSize; x++)
                            {
                                Vector2 areaOffset = areaOffsets[a];
                                currentImage[x + areaOffset.X, y + areaOffset.Y] = simulation[x, y];
                            }
                        }
                        */

                        output.WriteByte(1);
                        output.Write(BitConverter.GetBytes((int)updateStream.Length).ToArray());
                        output.Write(updateStream.ToArray());
                        output.Flush();

                        pixelUpdates++;
                    }
                    else
                    {
                        // Write motion update
                        output.WriteByte(2);
                        output.WriteByte(areaDirections[a]);
                        output.Flush();

                        motionUpdates++;
                    }
                }

                // child.Dispose();

                if (generatePreview)
                {
                    if (!Directory.Exists(@"preview"))
                    {
                        Directory.CreateDirectory(@"preview");
                    }

                    // Generate debug image
                    if (debugPreview)
                    {
                        Image<Rgb24> debugImage = currentImage.Clone();

                        for (int a = 0; a < transformedImgAreas.Length; a++)
                        {
                            if (updateAreas.Contains(a))
                            {
                                Vector2 areaOffset = areaOffsets[a];
                                // Write top line
                                for (int x = 0; x < MotionRectSize; x++)
                                {
                                    debugImage[areaOffset.X + x, areaOffset.Y] = new Rgb24(0, 255, 0);
                                }
                                // Write bottom line
                                for (int x = 0; x < MotionRectSize; x++)
                                {
                                    debugImage[areaOffset.X + x, areaOffset.Y + MotionRectSize - 1] = new Rgb24(0, 255, 0);
                                }
                                // Write left line
                                for (int y = 0; y < MotionRectSize; y++)
                                {
                                    debugImage[areaOffset.X, areaOffset.Y + y] = new Rgb24(0, 255, 0);
                                }
                                // Write right line
                                for (int y = 0; y < MotionRectSize; y++)
                                {
                                    debugImage[areaOffset.X + MotionRectSize - 1, areaOffset.Y + y] = new Rgb24(0, 255, 0);
                                }
                            }
                        }
                        
                        debugImage.SaveAsPng($"preview/{i.ToString("D4")}.png");
                    }
                    else
                    {
                        currentImage.SaveAsPng($"preview/{i.ToString("D4")}.png");
                    }
                }

                progress.Tick();

                progress.Message = $"Frame {progress.CurrentTick}: {motionUpdates} motion updates; {pixelUpdates} pixel updates";
            }

            if (audioFile != null)
            {
                // Write audio to file
                output.WriteByte(3);
                FileStream input = audioFile.OpenRead();
                byte[] buffer = new byte[input.Length];
                input.ReadExactly(buffer, 0, (int)input.Length);
                byte[] audioLength = BitConverter.GetBytes((int)input.Length).ToArray();
                output.Write(audioLength);
                output.Write(buffer);
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Done.");

            output.Close();
            fileStream.Close();
        }

        public static Image<Rgb24> ApplyAreaTransforms(Image<Rgb24> inputImage, byte[] areaDirections)
        {
            Image<Rgb24> outputImage = new Image<Rgb24>(Configuration.Default, inputImage.Width, inputImage.Height);
            int areaIdx = 0;
            int imageHeight = inputImage.Height;
            int imageWidth = inputImage.Width;

            bool[] definedPixels = new bool[imageHeight * imageWidth];

            for (int i = 0; i < definedPixels.Length; i++)
            {
                definedPixels[i] = false;
            }

            for (int y = 0; y < imageHeight; y += MotionRectSize)
            {
                for (int x = 0; x < imageWidth; x += MotionRectSize)
                {
                    byte areaDirection = areaDirections[areaIdx];
                    Vector2 movementVector = Directions[areaDirection];
                    for (int ym = y; ym < y + MotionRectSize; ym++)
                    {
                        for (int xm = x; xm < x + MotionRectSize; xm++)
                        {
                            int movedXm = xm + movementVector.X;
                            int movedYm = ym + movementVector.Y;

                            if (movedXm < 0 || movedYm < 0 || movedXm >= imageWidth - 1 || movedYm >= imageHeight - 1)
                            {
                                continue;
                            }
                            else
                            {
                                Vector2 position = new Vector2(movedXm, movedYm);
                                int pixelIdx = movedXm + imageWidth * movedYm;
                                if (definedPixels[pixelIdx] == false)
                                {
                                    outputImage[movedXm, movedYm] = inputImage[xm, ym];
                                    definedPixels[pixelIdx] = true;
                                }
                                else
                                {
                                    outputImage[movedXm, movedYm] = MixPixels(inputImage[xm, ym], outputImage[movedXm, movedYm]);
                                }
                            }
                        }
                    }
                    areaIdx++;
                }
            }

            // Fill in empty pixels
            int idx = 0;

            for (int y = 0; y < imageHeight; y++)
            {
                for (int x = 0; x < imageWidth; x++)
                {
                    if (definedPixels[idx] == false)
                    {
                        if (x > 1 && x < imageWidth - 1 && y > 1 && y < imageHeight - 1)
                        {
                            // Check avg value of neighbour pixels
                            List<Rgb24> neighbourPixelColors = new List<Rgb24>();
                            for (int n = 0; n < NeighbourPixel.Length; n++)
                            {
                                Vector2 vector2 = NeighbourPixel[n];
                                if (definedPixels[(x + vector2.X) + (y + vector2.Y) * imageWidth] == true)
                                {
                                    neighbourPixelColors.Add(outputImage[x + vector2.X, y + vector2.Y]);
                                }
                            }
                            if (neighbourPixelColors.Count > 0)
                            {
                                outputImage[x, y] = MixPixels(neighbourPixelColors);
                            }
                            else
                            {
                                outputImage[x, y] = inputImage[x, y];
                            }
                        }
                        else
                        {
                            outputImage[x, y] = inputImage[x, y];
                        }
                    }
                    idx++;
                }
            }

            return outputImage;
        }

        public static (byte, Vector2) CalculateBestAreaDirection(Area area1, Area area2, Image<Rgb24> image1, Image<Rgb24> image2)
        {
            int[] results = new int[Directions.Length];

            for (int i = 0; i < Directions.Length; i++)
            {
                Vector2 currentDirection = Directions[i];
                int diff = 0;

                for (int y = 0; y < MotionRectSize; y++)
                {
                    for (int x = 0; x < MotionRectSize; x++)
                    {
                        Rgb24? pixel1 = area1.GetPixel(x, y, MotionRectSize);
                        if (pixel1 == null)
                        {
                            pixel1 = GetPixel(image1, x + area1.OffsetX, y + area1.OffsetY);
                        }
                        Rgb24? pixel2 = area2.GetPixel(x + currentDirection.X, y + currentDirection.Y, MotionRectSize);
                        if (pixel2 == null)
                        {
                            pixel2 = GetPixel(image2, x + area2.OffsetX, y + area2.OffsetY);
                        }
                        int pixelDiff = PixelDiff((Rgb24)pixel1, (Rgb24)pixel2);
                        diff += pixelDiff;
                    }
                }

                results[i] = diff;
            }

            int lowestIdx = 0;
            int lowestValue = results[0];

            for (int i = 1; i < results.Length; i++)
            {
                int value = results[i];
                if (value < lowestValue)
                {
                    lowestIdx = i;
                    lowestValue = value;
                }
            }

            return ((byte)lowestIdx, Directions[(byte)lowestIdx]);
        }

        public static int ImageDiff(Image<Rgb24> image1, Image<Rgb24> image2)
        {
            int diff = 0;

            for (int y = 0; y < image1.Height; y++)
            {
                for (int x = 0; x < image1.Width; x++)
                {
                    diff += PixelDiff(image1[x, y], image2[x, y]);
                }
            }

            return diff;
        }

        public static int AreaDiff(Area area1, Area area2)
        {
            int diff = 0;
            int idx = 0;

            for (int y = 0; y < MotionRectSize; y++)
            {
                for (int x = 0; x < MotionRectSize; x++)
                {
                    diff += PixelDiff(area1.Data[idx], area2.Data[idx]);
                    idx++;
                }
            }

            return diff;
        }

        public static float CalculateWeight(Area area)
        {
            // Get brightest pixel
            Rgb24 brightestPixel = area.Data[0];
            int idx = 0;

            for (int y = 0; y < MotionRectSize; y++)
            {
                for (int x = 0; x < MotionRectSize; x++)
                {
                    Rgb24 pixel = area.Data[idx];
                    if (brightestPixel.R + brightestPixel.G + brightestPixel.B < pixel.R + pixel.G + pixel.B)
                    {
                        brightestPixel = pixel;
                    }
                    idx++;
                }
            }

            float brightness = (float)(brightestPixel.R + brightestPixel.G + brightestPixel.B) / 3f;

            return brightness / 255f;
        }

        public static void WriteIFrame(Stream output, Image<Rgb24> img)
        {
            output.WriteByte(0);

            MemoryStream memoryStream = new MemoryStream();

            JpegEnc.Encode(img, memoryStream);

            output.Write(BitConverter.GetBytes((int)memoryStream.Length).ToArray());
            output.Write(memoryStream.ToArray());

            output.Flush();
        }


        public static int PixelDiff(Rgb24 pixel1, Rgb24 pixel2)
        {
            return Math.Abs(pixel1.R - pixel2.R) + Math.Abs(pixel1.G - pixel2.G) + Math.Abs(pixel1.B - pixel2.B);
        }

        public static Rgb24 MixPixels(Rgb24 pixel1, Rgb24 pixel2)
        {
            return new Rgb24((byte)((pixel1.R + pixel2.R) / 2), (byte)((pixel1.G + pixel2.G) / 2), (byte)((pixel1.B + pixel2.B) / 2));
        }

        public static Rgb24 GetPixel(Image<Rgb24> img, int x, int y)
        {
            if (x < 0)
            {
                x = 0;
            }
            else if (x > img.Width - 1)
            {
                x = img.Width - 1;
            }
            if (y < 0)
            {
                y = 0;
            }
            else if (y > img.Height - 1)
            {
                y = img.Height - 1;
            }
            return img[x, y];
        }

        public static Rgb24 MixPixels(List<Rgb24> pixels)
        {
            int avgR = 0;
            int avgG = 0;
            int avgB = 0;

            for (int i = 0; i < pixels.Count; i++)
            {
                avgR += (int)pixels[i].R;
                avgG += (int)pixels[i].G;
                avgB += (int)pixels[i].B;
            }

            avgR = (int)((float)avgR / (float)pixels.Count);
            avgG = (int)((float)avgG / (float)pixels.Count);
            avgB = (int)((float)avgB / (float)pixels.Count);

            return new Rgb24((byte)(avgR), (byte)(avgG), (byte)(avgB));
        }

        public static Image<Rgb24> GetImage(FileInfo filePath)
        {
            return Image.Load<Rgb24>(filePath.OpenRead());
        }
    }
}