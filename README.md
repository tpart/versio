# Versio
An experimental video codec written in C#. It uses macroblocks to accurately describe moving pixel areas.

**Note: The design of this video codec is fundamentally flawed. Please try [Teal](https://gitlab.com/tpart/teal) instead.**

## Usage
### Encoding
At the moment, versio can only encode image sequences. First, convert your video into an image sequence using the following ffmpeg command:
```bash
ffmpeg -i video.mp4 -f image2 video4/image-%07d.png
```
Optionally, extract the audio using this ffmpeg command:
```bash
ffmpeg -i video.mp4 -vn audio.ogg
```
Next, start versio and encode your file:
```bash
dotnet run -- --encode video/ --audio audio.ogg --output output.vio
```
For more information, run:
```bash
dotnet run -- --help
```
### Decoding
A vio file can be decoded using the following command:
```bash
dotnet run -- --decode video.vio --output output-folder/
```
This will create an image sequence in output-foler as well as, if provided during the encoding process, an audio file. These files can be put back together using ffmpeg:
```bash
ffmpeg -framerate 24 -pattern_type glob -i '*.png' -c:v libvpx -pix_fmt yuv420p output.mkv
```
And, optionally, the audio file can also be added:
```bash
ffmpeg -i output.mkv -i audio.ogg -c copy -map 0:v:0 -map 1:a:0 output-audio.mkv
```

## License
This project is licensed under the GPL-3.0-or-later license.


Versio
Copyright (C) 2022 tpart

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.