namespace Versio
{
    public class Vector2
    {
        public int X;
        public int Y;

        public Vector2(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }
}